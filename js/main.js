//slick js
$(document).ready(function () {
  $(".c-banner__carousel").slick({
    autoplay: true,
    infinite: false,
    dots: true,
    arrows: false,
    draggable: false
  });
});
//header js
$(window).on("scroll", function () {
  var scroll = $(window).scrollTop();

  if (scroll >= 80) {
    $("#header").addClass("header-fixed");
  } else {
    $("#header").removeClass("header-fixed");
  }
});

//to top btn js
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function () {
  scrollFunction()
};
var toTopBtn = document.getElementById("toTopBtn");
function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    toTopBtn.style.display = "block";
  } else {
    toTopBtn.style.display = "none";
  }
}
// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
toTopBtn.addEventListener("click", topFunction);

